Usage
=====

.. argparse::
   :module: git_rsync.__main__
   :func: _parser
   :prog: git-rsync


Configuration file
------------------

.. code-block::
   :caption: Configuration file

   [<Repo description>]
   upstream=
      The upstream git repository to mirror references from.
   mirror=
      Downstream git repository to push the upstream mirrored reference to.
   refs=
      (Optional) A comma separated list of git refs to push from upstream to mirror repo.
      Note: By default, only 'heads' and 'tags' are mirrored.


.. code-block::
   :caption: Configuration file example

   [musl]
   upstream=git://git.musl-libc.org/musl
   mirror=git@gitlab.com:csmos/mirrors/musl.git
   refs=+refs/heads/*:refs/heads/*,+refs/tags/*:refs/tags/*

   [klibc]
   upstream=git://git.kernel.org/pub/scm/libs/klibc/klibc.git
   mirror=git@gitlab.com:csmos/mirrors/klibc.git
   refs=+refs/heads/*:refs/heads/*
