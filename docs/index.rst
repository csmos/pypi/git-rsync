.. include:: ../README.rst
   :end-before: github-only

.. _Contributor Guide: contributing.html
.. _Usage: usage.html

.. toctree::
   :hidden:
   :maxdepth: 1

   usage
   reference
   devguide
   contributing
   Code of Conduct <codeofconduct>
   License <license>
   Changelog <https://gitlab.com/csmos/pypi/git-rsync/releases>
