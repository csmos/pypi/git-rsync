#!/bin/bash
# SPDX-License-Identifier: GPL-2.0-only
#
# Build git-rsync project.
#
# Copyright (C) 2022 Daniel Gomez
# Author: Daniel Gomez <dagmcr@gmail.com>
set -e

docker run --rm \
-w $PWD \
-v $PWD:$PWD \
--entrypoint /opt/poetry/bin/poetry \
registry.gitlab.com/csmos/containerhub/python-dev \
build

sudo chown -R $USER:$USER dist/
