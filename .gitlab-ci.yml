---
stages:
  - lint
  - package-registry:pypi
  - container-registry
  - docs

lint:
  stage: lint
  image: registry.gitlab.com/csmos/containerhub/python-dev:latest
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
  script:
    - nox -rs pre-commit # lint
    - nox -rs mypy-3.10 # type-check

.package-registry:pypi:
  stage: package-registry:pypi
  image: registry.gitlab.com/csmos/containerhub/python-dev:latest
  rules:
    - if: $CI_COMMIT_TAG
  variables:
    PYPACKAGE: package
  script:
    - set -xev
    - >
      poetry config repositories.gitlab
      https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/packages/pypi
    - >
      poetry config http-basic.gitlab
      gitlab-ci-token "$CI_JOB_TOKEN"
    - poetry install
    - poetry run $PYPACKAGE --help
    - poetry run $PYPACKAGE --version
    - >
      poetry publish
      --build
      --repository gitlab
      --verbose

package-registry:
  extends: .package-registry:pypi
  variables:
    PYPACKAGE: git-rsync
  artifacts:
    paths:
      - dist

package-registry:latest:
  extends: .package-registry:pypi
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  before_script:
    - sed -i 's/__version.*/__version__ = "0"/g' src/git_rsync/__init__.py
    - sed -i 's/^version = ".*/version = "0"/g' pyproject.toml
    - poetry install
    - export PKG_VERSION=$(poetry run $PYPACKAGE --version | cut -d" " -f2)
    - >
      GITLAB_TOKEN=$GITLAB_TOKEN
      CI_PROJECT_ID=$CI_PROJECT_ID
      PKG_VERSION=$PKG_VERSION
      bash ci/delete_pkg.sh
  variables:
    PYPACKAGE: git-rsync
  artifacts:
    paths:
      - dist

.container-registry:
  stage: container-registry
  image: docker
  services:
    - docker:dind
  rules:
    - if: $CI_COMMIT_TAG
  needs: ["package-registry"]
  variables:
    DOCKER_DIR: $CI_PROJECT_DIR/dockerfiles/alpine/latest
    TARGET_IMAGE: $CI_COMMIT_TAG
  script:
    - set -xev
    - env | sort
    - cat /proc/cpuinfo
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
    - docker build -t $CI_REGISTRY_IMAGE:$TARGET_IMAGE -f $DOCKER_DIR/Dockerfile .
    - docker tag $CI_REGISTRY_IMAGE:$TARGET_IMAGE $CI_REGISTRY_IMAGE:$TARGET_IMAGE
    - docker push $CI_REGISTRY_IMAGE:$TARGET_IMAGE

container-registry:
  extends: .container-registry

container-registry:latest:
  extends: .container-registry
  needs: ["package-registry:latest"]
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  variables:
    TARGET_IMAGE: latest

docs:
  stage: docs
  image: registry.gitlab.com/csmos/containerhub/python-dev:latest
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_COMMIT_TAG
  script:
    - nox -rs docs-build
  artifacts:
    paths:
      - docs/_build

pages:
  stage: docs
  image: registry.gitlab.com/csmos/containerhub/python-dev:latest
  needs: ["docs"]
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_COMMIT_TAG
  script:
    - mv docs/_build public
  artifacts:
    paths:
      - public
