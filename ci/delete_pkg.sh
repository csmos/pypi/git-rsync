#!/bin/bash
# SPDX-License-Identifier: GPL-2.0-only
#
# Script to delete latest release candidate package from GitLab Package
# Registry.
#
# Copyright (C) 2022 Daniel Gomez
# Author: Daniel Gomez <dagmcr@gmail.com>

last_id_pkg=$(\
  curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
  "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages" | python3 -c \
  "import sys, json; res=(json.load(sys.stdin)); print(res[len(res)-1]['id'])"
  )
last_version_pkg=$(\
  curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
  "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages" | python3 -c \
  "import sys, json; res=(json.load(sys.stdin)); print(res[len(res)-1]['version'])"
  )

echo "Project: $CI_PROJECT_ID"
echo "Package id: $last_id_pkg"
echo "Package version: $last_version_pkg"
echo "Package version requested: $PKG_VERSION"
return 0

if [[ "${last_version_pkg}" == *"${PKG_VERSION}"* ]]; then
    set -x
    echo "Deleting package ${last_id_pkg} with ${PKG_VERSION} version"
    curl --request DELETE --header \
    "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/${last_id_pkg}"
else
    echo "Package ${last_id_pkg} with version ${PKG_VERSION} not found"
fi
